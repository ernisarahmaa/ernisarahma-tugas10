import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {TextRegular, TextBold, TextMedium} from '../../component/global';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import ModalBottom from '../../component/modal/ModalBottom';
import ModalCenter from '../../component/modal/ModalCenter';
import {NumberFormatter} from '../../utils/Helper';
import {validateEmail} from '../../utils/Helper';
import LoginComponent from '../../component/section/auth/LoginComponent';

const Login = ({navigation, route}) => {
  const [modalBottom, setModalBottom] = useState(false);
  const [modalCenter, setModalCenter] = useState(false);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [hidepassword, setHidePassword] = useState(true);

  const onLogin = () => {
    const contohEmail = 'codemaster@gmail.com';
    if (!validateEmail(contohEmail)) {
      //akan ada pesan error karena format email salah
    } else {
      //format email benar
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <LoginComponent
        navigation={navigation}
        email={email}
        setEmail={text => setEmail(text)}
        password={password}
        setPassword={text => setPassword(text)}
        hidePassword={hidepassword}
        setShowPassword={() => setHidePassword(prevState => !prevState)}
        onPressLogin={() => onLogin()}
      />

      <TouchableOpacity
        onPress={() => setModalBottom(true)}
        style={styles.btnShowModalBottom}>
        <TextBold text="Modal Bottom" size={16} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setModalCenter(true)}
        style={[
          styles.btnShowModalBottom,
          {backgroundColor: Colors.DEEPORANGE},
        ]}>
        <TextBold text="Modal Center" size={16} color="#fff" />
      </TouchableOpacity>
      <ModalBottom
        show={modalBottom}
        onClose={() => setModalBottom(false)}
        title="Modal Bottom">
        <View>
          <TextBold text="ini text bold" />
          <TextRegular
            text={`${NumberFormatter(10000000, 'Rp. ')}`}
            size={16}
            color={Colors.BLACK}
            style={{
              marginTop: 20,
              marginLeft: 20,
            }}
          />
          <TextMedium text="ini text medium" />
        </View>
      </ModalBottom>
      <ModalCenter
        show={modalCenter}
        onClose={() => setModalCenter(false)}
        title="Modal Center">
        <View>
          <TextBold text="ini text bold" />
          <TextRegular
            text={`${NumberFormatter(10000000, 'Rp. ')}`}
            size={16}
            color={Colors.BLACK}
            style={{
              marginTop: 20,
              marginLeft: 20,
            }}
          />
          <TextMedium text="ini text medium" />
        </View>
      </ModalCenter>
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default Login;
